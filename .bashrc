#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PS1='┌────(\u@\h)──(\w) \n└─$ '

alias ls='ls -a --color=auto'
alias ll='ls -lah --color=auto'
alias get='sudo pacman -S'
alias del='sudo pacman -Rns'
alias update='sudo pacman -Su --noconfirm'
alias upgrade='sudo pacman -Syu --noconfirm'
alias syncdb='sudo pacman -Sy'
alias clean='sudo pacman -Sc'
alias search='pacman -Ss'
alias add='sudo pacman -U'
alias list='pacman -Qq | less'
alias mac='macchanger -A wlp3s0 | grep Current ; macchanger -A eno1 | grep Current'
alias so='source'
alias neo='neofetch'
alias top='htop'
alias py='python3'
alias hx='helix'

export HISTSIZE=10
export EDITOR=helix

export PATH=/home/robot/.local/bin:$PATH
